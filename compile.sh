#!/bin/bash
# This script will be used to compile the simulator application
 
#Change directory to the folder containing source files.
cd Java_Workspace/CSLP_dev/src/ParserPackage

#Store all java filenames in a file
find -name "*.java" > sources.txt

#Create a bin directory.
mkdir -p ../../bin

#Compile all java programs, and store them in the bin directory. 
javac -d ../../bin @sources.txt

#Return to original directory.
cd ../../../..


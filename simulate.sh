#!/bin/bash
# This script will be used to launch simulations 

## \nUsage Information:
## \n ./simulate.sh <input_file_name> \n
## \nExperimentation can be performed via the 'experiment' keyword 
## \nNote:Detailed output is disabled for experimentation.Only summary statistics are printed.
## \n


#Capture usage information.
help=$(grep "^## " "${BASH_SOURCE[0]}" |cut -c 4-)

#Display usage information if the number of arguments is 0. 
if [ "$#" -eq 0 ]; then
	echo -e $help

   	
else

#Copy the input file to a location easily accessible by the java programs
cat $1 >./Java_Workspace/CSLP_dev/bin/input.txt

#Change working directory and launch java program
cd ./Java_Workspace/CSLP_dev/bin
java ParserPackage/RunApp input.txt parsed.txt

#Change working directory back 
cd ../../..

fi



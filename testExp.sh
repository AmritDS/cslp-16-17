#!/bin/bash
# This script will be used to test the simulator application

#Define colors used
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

#Compile java programs
source compile.sh

#Test for missing tokens

#Change directory to the folder containing input files.
cd tests/validScripts_Exp

#Store all input filenames in a file
find -name "*.txt" > ../../testFiles.txt

cd ../..

#Run simulate.sh for each file and report results.
while read line
do
     source simulate.sh tests/validScripts_Exp/"${line:2}" >output.txt
     
     if grep -Fq "lorry" output.txt
     then
            echo "${line:2} Detailed output printed -${red}Fail${reset}"  
     elif grep -Fq "bag" output.txt
     then
            echo "${line:2} Detailed output printed -${red}Fail${reset}"     
     elif grep -Fq "load" output.txt
     then
            echo "${line:2} Detailed output printed -${red}Fail${reset}"     
     else
            echo "${line:2} Only summary statistics printed - ${green}Pass${reset}"
            
fi
done < testFiles.txt

printf '\n'


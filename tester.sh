#!/bin/bash
# This script will be used to test the simulator application
source testParser.sh
printf '\n\n'
source testGraph.sh
printf '\n\n'
source testOutput.sh
printf '\n\n'
source testExp.sh
printf '\n\n'
source testFunctions.sh

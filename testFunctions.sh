#!/bin/bash
# This script will be used to test output of the simulator application

#Define colors used
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0` 
source compile.sh

#Change directory to the folder containing source files.
cd Java_Workspace/CSLP_dev/bin

echo "Testing nearest neighbor heuristics for route planning..."
echo "Testing return to depot scheduling ends at the depot..."

java ParserPackage/Graph_UnitTests >output1.txt

if grep -Fq "Error" output1.txt 
then
   cat javaTests_output.txt
   echo "Tests unsucessful -${red}Fail${reset}"
else
   echo "Tests sucessful- ${green}Pass${reset}" 
fi

printf '\n'

echo "Testing lorry scheduling..."
echo "Testing the mergelists function to merge lists of events..."


java ParserPackage/Setup_UnitTests >output2.txt

if grep -Fq "Error" output2.txt 
then
   cat javaTests_output.txt
   echo "Tests unsucessful -${red}Fail${reset}"
else
   echo "Tests sucessful- ${green}Pass${reset}" 
fi


cd ../../..

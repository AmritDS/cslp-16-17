#!/bin/bash
# This script will be used to test output of the simulator application

#Define colors used
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0` 

#Change directory to the folder containing source files.
cd Java_Workspace/TestOutput/src/TestFiles

#Store all java filenames in a file
find -name "*.java" > sources.txt

#Create a bin directory.
mkdir -p ../../bin

#Compile all java programs, and store them in the bin directory. 
javac -d ../../bin @sources.txt

#Return to original directory.
cd ../../../..

#Compile the program and store the output in the file output.txt 
source compile.sh


#Test output for all Files in ValidScripts_NotExp

#Change directory to the folder containing input files.
cd tests/validScripts_NotExp

#Store all input filenames in a file
find -name "*.txt" > ../../testFiles.txt

cd ../..

#Run simulate.sh for each file and test the output.
while read line
do
     source simulate.sh tests/validScripts_NotExp/"${line:2}" >output.txt
     echo "${line:2} Testing Output------------------------------------------"

     cat output.txt >./Java_Workspace/TestOutput/bin/output.txt
     cd ./Java_Workspace/TestOutput/bin
     java RunTests output.txt >../../../javaTests_output.txt


     #Change working directory back  
     cd ../../..

     ####################################################################################################################
     #Java tests.
     echo "Testing time only moves forward..."
     echo "Testing bin volume only changes as a concequence of garbage disposal or lorry service..."


     if grep -Fq "Error" javaTests_output.txt 
     then
      cat javaTests_output.txt
      echo "Tests on output unsucessful -${red}Fail${reset}"
     else
      echo "Tests on output sucessful- ${green}Pass${reset}"       
      
     
     
fi
done < testFiles.txt

printf '\n'








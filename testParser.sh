#!/bin/bash
# This script will be used to test the simulator application

#Define colors used
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

#Compile java programs
source compile.sh

#Test for missing tokens

#Change directory to the folder containing input files.
cd tests/missingInfo

#Store all input filenames in a file
find -name "*.txt" > ../../testFiles.txt

cd ../..

#Run simulate.sh for each file and report results.
while read line
do
     source simulate.sh tests/missingInfo/"${line:2}" >results.txt
     
     if grep -Fq "Error" results.txt
     then
            echo "${line:2} token absence detected - ${green}Pass${reset}"     
     else
            echo "${line:2} token absence not detected -${red}Fail${reset}"
fi
done < testFiles.txt

printf '\n'

######################################################################################################################
#Test for Incorrect areas format

#Change directory to the folder containing input files.
cd tests/incorrectAreasFormat

#Store all input filenames in a file
find -name "*.txt" > ../../testFiles.txt

cd ../..

#Run simulate.sh for each file and report results.
while read line
do
     source simulate.sh tests/incorrectAreasFormat/"${line:2}" >results.txt
     
     if grep -Fq "Error" results.txt
     then
            echo "${line:2} incorrect format detected - ${green}Pass${reset}"     
     else
            echo "${line:2} incorrect format not detected -${red}Fail${reset}"
fi
done < testFiles.txt

printf '\n'

####################################################################################################################
#Test for Incorrect Magnitudes

#Change directory to the folder containing input files.
cd tests/incorrectMagnitudes

#Store all input filenames in a file
find -name "*.txt" > ../../testFiles.txt

cd ../..

#Run simulate.sh for each file and report results.
while read line
do
     source simulate.sh tests/incorrectMagnitudes/"${line:2}" >results.txt
     
     if grep -Fq "Error" results.txt
     then
            echo "${line:2} incorrect magnitude detected - ${green}Pass${reset}"     
     else
            echo "${line:2} incorrect magnitude not detected -${red}Fail${reset}"
fi
done < testFiles.txt

printf '\n'

####################################################################################################################
#Test Sanity checks

#Change directory to the folder containing input files.
cd tests/sanityCheck

#Store all input filenames in a file
find -name "*.txt" > ../../testFiles.txt

cd ../..

#Run simulate.sh for each file and report results.
while read line
do
     source simulate.sh tests/sanityCheck/"${line:2}" >results.txt
     
     if grep -Fq "Warning" results.txt
     then
            echo "${line:2} warning printed - ${green}Pass${reset}"     
     else
            echo "${line:2} no warning printed -${red}Fail${reset}"
fi
done < testFiles.txt

printf '\n'

####################################################################################################################
#Test that an no errors or warnings occur on valid scripts

#Change directory to the folder containing input files.
cd tests/validInput

#Store all input filenames in a file
find -name "*.txt" > ../../testFiles.txt

cd ../..

#Run simulate.sh for each file and report results.
while read line
do
     source simulate.sh tests/validInput/"${line:2}" >results.txt
     
     if grep -Fq "Warning" results.txt
     then
            echo "${line:2} warning printed - ${red}Fail${reset}"     
     elif grep -Fq "Error" results.txt
     then
            echo "${line:2} error detected -${red}Fail${reset}"
     else
 	    echo "${line:2} parsed successfully -${green}Pass${reset}"
fi
done < testFiles.txt

printf '\n'

####################################################################################################################
#Test other warnings

#Change directory to the folder containing input files.
cd tests/otherWarnings

#Store all input filenames in a file
find -name "*.txt" > ../../testFiles.txt

cd ../..

#Run simulate.sh for each file and report results.
while read line
do
     source simulate.sh tests/otherWarnings/"${line:2}" >results.txt
     
     if grep -Fq "Warning" results.txt
     then
            echo "${line:2} warning printed - ${green}Pass${reset}"     
     else
 	    echo "${line:2} warning not printed -${red}Fail${reset}"
fi
done < testFiles.txt

printf '\n'

####################################################################################################################
#Test other errors

#Change directory to the folder containing input files.
cd tests/otherErrors

#Store all input filenames in a file
find -name "*.txt" > ../../testFiles.txt

cd ../..

#Run simulate.sh for each file and report results.
while read line
do
     source simulate.sh tests/otherErrors/"${line:2}" >results.txt
     
     if grep -Fq "Error" results.txt
     then
            echo "${line:2} error detected - ${green}Pass${reset}"     
     else
 	    echo "${line:2} error not detected -${red}Fail${reset}"
fi
done < testFiles.txt



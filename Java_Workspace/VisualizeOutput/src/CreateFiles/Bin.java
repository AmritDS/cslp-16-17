package CreateFiles;
import java.io.*;
public class Bin 
{
	int time;
	File file;
	public Bin(String fileName)throws IOException
	{
		this.file=new File(fileName);
		FileWriter fout=new FileWriter(file);	//Erase contents of file if it already exists.
		fout.close();

		time=0;
	}

	public void setTime(int time)
	{
		this.time=time;
	}
	
	public int getTime()
	{
		return time;
	}
	
	public void write(int delay)throws IOException
	{
		FileWriter fout=new FileWriter(file,true);
		fout.write(delay+"\n");
		fout.close();
	}
	
}

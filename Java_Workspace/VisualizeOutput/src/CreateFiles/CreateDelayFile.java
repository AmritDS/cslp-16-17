package CreateFiles;
import java.io.*;
import java.util.*;
public class CreateDelayFile 
{
	//Creates a delay file for each bin.
	public static void main(String[] args)throws IOException
	{
		File file=new File("output.txt");
		FileReader fin=new FileReader(file);
		BufferedReader br=new BufferedReader(fin);
		
		HashMap<String,Bin> bins=new HashMap<String,Bin>();
		
		
		String line;
		String[] lineSplit;
		int eventTime;
		
		boolean flag;
		
		while((line=br.readLine())!=null)
		{
			
			lineSplit=new String[line.split(" ").length];
			lineSplit=line.split(" ");
			
			if(lineSplit[2].equals("bag"))
			{
				flag=false;
				
				Set<String> keys=bins.keySet();
				for(String k:keys)
				{
					if(lineSplit[lineSplit.length-1].equals(k))
					{
						flag=true;
						break;
					}
					
				}
				
				
				if(!flag)
				{
					bins.put(lineSplit[lineSplit.length-1],new Bin(lineSplit[lineSplit.length-1]+"Delay.txt"));
				}
				
				
				eventTime=getTime(lineSplit[0]);
				bins.get(lineSplit[lineSplit.length-1]).write(eventTime-bins.get(lineSplit[lineSplit.length-1]).getTime());
				bins.get(lineSplit[lineSplit.length-1]).setTime(eventTime);
				
			}
		}
		
		
		br.close();
		System.out.println("Done");
	}
	
	//Converts the formated time back to seconds.
	public static int getTime(String str)
	{
		String[] values=str.split(":");
		int[] vals=new int[4];
		for(int i=0;i<4;i++)
			vals[i]=Integer.parseInt(values[i]);
		
		int time=vals[0]*24*3600+vals[1]*3600+vals[2]*60+vals[3];
		return time;
	}
	
}

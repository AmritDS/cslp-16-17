import java.io.*;
public class RunTests 
{
	public static void main(String args[])throws IOException
	{
		File file=new File(args[0]);
		FileReader fin=new FileReader(file);
		BufferedReader br=new BufferedReader(fin);
		
		String line;
		String linePrev="";
		long timePrev=0;
		long time=0;
		String[] timeFormat=new String[4];
		long[] timeF=new long[4];
		boolean mustBeLorryService=false;
		
		
		while((line=br.readLine())!=null)
		{
			
			if(line.replace(" ","").matches(""))
			{
				break;
			}
			
			
			try
			{
				//Parse the time of the event
				timeFormat=line.split(" ")[0].split(":");
				
				for(int i=0;i<4;i++)
					timeF[i]=Long.parseLong(timeFormat[i]);
				
				time=timeF[0]*24*3600+timeF[1]*3600+timeF[2]*60+timeF[3];
				
				//Current time is smaller than the previous time
				if(time-timePrev<0)
				{
					System.out.println("Error! Time moving backwards");
					System.exit(0);
				}
				
				
				if(mustBeLorryService)
				{
					if(line.split(" ")[4].equals("lorry"))
						mustBeLorryService=false;
					else
					{
						System.out.println("Error! The load of a bin changes without garbagedisposal or lorry service.");
						System.exit(0);
					}
				}
				
				//load of bin only changes when a garbage bag is disposed and check that the bin has not overflowed.
				if(line.split(" ")[2].equals("load")&&line.split(" ")[4].equals("bin"))
				{
					
					try{
					//Check if the previous line was a bag disposal, and that the area id and bin id correspond.
					if(linePrev.split(" ")[2].equals("bag")&&linePrev.split(" ")[10].equals(line.split(" ")[5]))
					{
						//Do nothing it was a garbage disposal.
					}
					}
					catch(Exception e)//It was not a garbage disposal.
					{
						mustBeLorryService=true;
					}
					
					
				}
				
				
				
				timePrev=time;
				linePrev=line;
				
				
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			
			
			
			
		}
		br.close();
		
	}
}

package ParserPackage;

import java.io.*;
import java.util.HashMap;
import java.util.LinkedList;
public class Parser
{
	//This list of keywords triggers the related parsing action.
	static final String[] tokens={"lorryVolume","lorryMaxLoad","binServiceTime","binVolume","disposalDistrRate","disposalDistrShape","bagVolume","bagWeightMin","bagWeightMax","noAreas","serviceFreq","stopTime","warmUpTime"};
	
	
		
	//Main Parsing Functions, called externally.
	//****************************************************************************************************************************************************//
	
	//This function takes a raw input file and removes comments and blank lines, storing the result in a new file
    public static void removeMetaData(File file1,File file2)throws IOException
	{
		FileReader in=new FileReader(file1);
		BufferedReader br=new BufferedReader(in);
		
		FileWriter out=new FileWriter(file2);
		
		String temp;
		while((temp=br.readLine())!=null)
		{
		
		if(isBlankComment(temp))
			continue;
		
		out.write(temp+"\n");
		
		}
		
		
		in.close();
		out.close();
		br.close();
		
		
	}
   
    //This function checks if a given input file contains an experiment.
	public static boolean isExperiment(String temp)
	{
		
			if(temp.matches("(.*)experiment(.*)"))
			{
				return true;
			}
		
		return false;
		
	}
	
	//This function extracts useful information from a file and stores it in a SimEnv object,returning this object.
	public static SimEnv getInfo(File file)throws IOException
	{
		SimEnv currentEnv=new SimEnv();
		
		boolean[] usedTokens=new boolean[tokens.length];
		
		File unrecognized=new File("unrecognised.txt");
		
		FileReader in=new FileReader(file);
		BufferedReader br=new BufferedReader(in);
		
		FileWriter out=new FileWriter(unrecognized);
		
		String temp;
		boolean flag;
		
		br.mark(1000); //Set buffer mark
		while((temp=br.readLine())!=null)
		{
			flag=false;
			
			for(int i=0;i<tokens.length;i++)
			{
				if(temp.matches("^"+tokens[i]+"(.*)")&&!usedTokens[i])
				{
					flag=true;
					usedTokens[i]=true;
					br.reset(); //Capture the entire section
					parseIt(tokens[i],br,currentEnv); //Will move the pointer for the buffer ahead while parsing.
				}
			}
			
			
			if(!flag)			//Unrecognized line
				out.write(temp);
			
			br.mark(1000);// Set the buffer mark.
			
		}
		
		
		in.close();
		out.close();
		br.close();
		
		boolean unrecog=false;
		
		//Check if there was any unrecognized data and print to screen.
		FileReader fin=new FileReader(unrecognized);
		BufferedReader fbr=new BufferedReader(fin);
		
		if((temp=fbr.readLine())!=null)
		{
			unrecog=true;
			System.out.println("Warning! The input file contains unrecognized data :\n");
			System.out.println(temp);
			
			while((temp=fbr.readLine())!=null)
			{
				System.out.println(temp);
				
			}
		}
		
		
		fbr.close();
		
		//Set to true if required information is missing.
		flag=false;
		
		for(int i=0;i<usedTokens.length;i++)
		{
			if(usedTokens[i]==false&&!tokens[i].equals("serviceFreq")) //Ignore serviceFreq as this is an overload parameter.
				flag=true;
				
		}
		
		if(flag)
		{
			System.out.println("Error! Required Information missing or unrecognized.");
			System.out.println("The following keywords cannot be detected in the given input script:");
			
			for(int i=0;i<usedTokens.length;i++)
			{
				if(usedTokens[i]==false)
					System.out.println(tokens[i]);
					
			}
			
			//Terminate the program.
			System.exit(0);
		}
		
		
	
		if(unrecog)
			System.out.println("\nRequired data present, proceeding anyway...");
		
		
		//The binVolume needs to be set for each bin. (This was not done while creating bins because the binVolume parameter may not have been defined.)
		Area area;
		for(int i=0;i<currentEnv.getAreas().size();i++)
		{
			area=currentEnv.getAreas().get(i);
			for(int j=0;j<area.getNoBins();j++)
			{
				area.getBins().get(j).setMaxVolume(currentEnv.getBinVolume());
			}
		}
		
		
		return currentEnv;
				
	}

	//This function performs a sanity check on the parsed data and issues warnings if the data is somehow unrealistic.
	public static void sanityCheck(SimEnv currentEnv)
	{
		//true if a warning is encountered.
		boolean flag=false;
		
		//Error in ranges.
		if(currentEnv.getBagWeightMax()<currentEnv.getBagWeightMin())
		{
			System.out.println("Error! Bag max weight smaller than bag min weight");
			System.exit(0);
		}
		
		//Lorry volume smaller than bin volume
		if(currentEnv.getLorryVolume()<currentEnv.getBinVolume()){flag=true;
			System.out.println("Warning! Bin Volume exceeds lorry Volume.");}
		
		//Lorry volume smaller than bag volume
		if(currentEnv.getLorryMaxLoad()<currentEnv.getBagVolume()){flag=true;
			System.out.println("Warning! Bag Volume exceeds lorry Volume.");}
		
		//Max bag weight greater than max bin/lorry weight.
		if(currentEnv.getLorryVolume()<currentEnv.getBagWeightMax()){flag=true;
			System.out.println("Warning! Max bag weight exceeds lorry capacity.");}
		
		//bag volume greater than bin volume.
		if(currentEnv.getBinVolume()<currentEnv.getBagVolume()){flag=true;
			System.out.println("Warning! Bag Volume exceeds bin Volume.");}
		
		//Warm up time greater than simulation time.
		if(currentEnv.getStopTime()<currentEnv.getWarmUpTime()){flag=true;
			System.out.println("Warning! Warm up time exceeds stop time.");}
		
		//Number of service areas=0
		if(currentEnv.getNoAreas()==0){flag=true;
			System.out.println("Warning! Number of service areas=0");}
		
		//disposalDistrRate is 0
		if(currentEnv.getDisposalDistrRate()==0&&!currentEnv.isExp())
			System.out.println("Warning! Disposal rate=0");
		
		for(int i=0;i<currentEnv.getNoAreas();i++)
		{
			//Number of bins in an area =0
			if(currentEnv.getAreas().get(i).getNoBins()==0){flag=true;
				System.out.println("Warning! Number of bins in area "+currentEnv.getAreas().get(i).getAreaIdx()+" =0");}
			
			//Number of bins in an area =1
			if(currentEnv.getAreas().get(i).getNoBins()==1){flag=true;
				System.out.println("Warning! Number of bins in area "+currentEnv.getAreas().get(i).getAreaIdx()+" =1");}
		
			//Disposal rate exceeds service rate
			if(currentEnv.getDisposalDistrRate()<currentEnv.getAreas().get(i).getServiceFreq()&&!currentEnv.isExp()){flag=true;
				System.out.println("Warning! Bin service time exceeds disposal rate for area "+currentEnv.getAreas().get(i).getAreaIdx());}
		
		
			//Threshold value for an area =0
			if(currentEnv.getAreas().get(i).getThresholdVal()==0)
				System.out.println("Warning! Threshold value=0 for area "+currentEnv.getAreas().get(i).getAreaIdx());
			
			//Service frequency for an area=0
			if(currentEnv.getAreas().get(i).getServiceFreq()==0&&!currentEnv.isExp())
				System.out.println("Warning! serviceFreq is 0 for area "+currentEnv.getAreas().get(i).getAreaIdx());
		}
		
		
		if(flag)
			System.out.println("Proceeding anyway...");
	}
	
	
	//Functions parsing related tokens.
	//****************************************************************************************************************************************************//
		
	
	//Parses lorryVolume.
	public static void lorVol(BufferedReader br,SimEnv currentEnv)
	{
		currentEnv.setLorryVolume(genericIntParser(br,"lorryVolume"));
		
	}
	
	//Parses lorryMaxload
	public static void lorMaxL(BufferedReader br,SimEnv currentEnv)
	{
		currentEnv.setLorryMaxLoad(genericIntParser(br,"lorryMaxLoad"));
	}
	
	//Parses binServiceTime
	public static void binST(BufferedReader br,SimEnv currentEnv)
	{
		currentEnv.setBinServiceTime(genericIntParser(br,"binServiceTime"));
	}
	
	//Parses binVolume
	public static void binV(BufferedReader br,SimEnv currentEnv)
	{
		currentEnv.setBinVolume(genericFloatParser(br,"binVolume"));
	}
	
	//Parses disposalDistrRate
	public static void dispDR(BufferedReader br,SimEnv currentEnv) //Saves as per hour.
	{
		try
		{
			br.mark(1000);
			if(isExperiment(br.readLine()))
			{
				br.reset();
				currentEnv.setDisposalDistrRateExp(genericFloatParserExp(br,"disposalDistrRate"));
				currentEnv.setExp(true);
			}
			else
			{
				br.reset();
				currentEnv.setDisposalDistrRate(genericFloatParser(br,"disposalDistrRate"));
			}
		
		
		}
		catch(Exception e)
		{
			System.out.println("IO Exception parsing disposalDistrRate");
			System.exit(0);
		}
		
		
	}
	
	//Parses disposalDistrShape
	public static void dispDS(BufferedReader br,SimEnv currentEnv)
	{
		try
		{
			br.mark(1000);
			if(isExperiment(br.readLine()))
			{
				br.reset();
				currentEnv.setDisposalDistrShapeExp(genericIntParserExp(br,"disposalDistrShape"));
				currentEnv.setExp(true);
			}
			else
			{
				br.reset();
				currentEnv.setDisposalDistrShape(genericIntParser(br,"disposalDistrShape"));
			}
		
		
		}
		catch(Exception e)
		{
			System.out.println("IO Exception parsing disposalDistrRate");
			System.exit(0);
		}
	}
	
	//Parses bagVolume
	public static void bagV(BufferedReader br,SimEnv currentEnv)
	{
		currentEnv.setBagVolume(genericFloatParser(br,"bagVolume"));
	}
	
	//Parses bagWeightMin
	public static void bagWmin(BufferedReader br,SimEnv currentEnv)
	{
		currentEnv.setBagWeightMin(genericFloatParser(br,"bagWeightMin"));
	}
	
	//Parses bagWeightMax
	public static void bagWmax(BufferedReader br,SimEnv currentEnv)
	{
		currentEnv.setBagWeightMax(genericFloatParser(br,"bagWeightMax"));
	}
	
	//Parses stopTime
	public static void stopT(BufferedReader br,SimEnv currentEnv)
	{
		//Store value in seconds.
		currentEnv.setStopTime((long)genericFloatParser(br,"stopTime")*3600);
	}
	
	//Parses warmUpTime
	public static void warmUT(BufferedReader br,SimEnv currentEnv)
	{
		//Store value in seconds.
		currentEnv.setWarmUpTime((long)genericFloatParser(br,"warmUpTime")*3600);
	}
	
	
	//Parses serviceFreq (when it is used for experimentation/to overload area frequencies)
	public static void serviceFreq(BufferedReader br,SimEnv currentEnv)
	{
		try
		{
			br.mark(1000);
			if(isExperiment(br.readLine()))
			{
				br.reset();
				currentEnv.setServiceFreqExp(genericFloatParserExp(br,"serviceFreq"));
				currentEnv.setExp(true);
			}
			else
			{
				System.out.println("If this is not an experiment, serviceFreq must be defined for each area.");
				System.out.println("Use the experiment key word to run experiments.");
				System.exit(0);
			}
		
		
		}
		catch(Exception e)
		{
			System.out.println("IO Exception parsing disposalDistrRate");
			System.exit(0);
		}
	}
	
	
	//Parses noAreas and area information that follows.
	@SuppressWarnings("unchecked")
	public static void numAreas(BufferedReader br,SimEnv currentEnv)
	{
		try
		{
		int noAreas=genericIntParser(br,"noAreas");
		String line;
		int temp_areaIdx=0;
		float temp_serviceFreq=0;
		float temp_threshold=0;
		int temp_noBins=0;
		HashMap<Integer,Integer>[] temp_adjList;
		LinkedList<Bin> temp_bins=new LinkedList<Bin>();
		int weight;
		String[] row;
		
		for(int i=0;i<noAreas;i++)
		{
			
						
			try
			{
				line=br.readLine();
				if(!line.split(" ")[0].equalsIgnoreCase("areaIdx"))
					throw (new Exception());
				if(!line.split(" ")[2].equalsIgnoreCase("serviceFreq"))
					throw (new Exception());
				if(!line.split(" ")[4].equalsIgnoreCase("thresholdVal"))
					throw (new Exception());
				if(!line.split(" ")[6].equalsIgnoreCase("noBins"))
					throw (new Exception());
				
				temp_areaIdx=Integer.parseInt(line.split(" ")[1]);
				temp_serviceFreq=Float.parseFloat(line.split(" ")[3]);
				temp_threshold=Float.parseFloat(line.split(" ")[5]);
				temp_noBins=Integer.parseInt(line.split(" ")[7]);
				row=new String[temp_noBins+1];
				
				if(temp_areaIdx<0)
				{
					System.out.println("Error! areaIdx must be non negative");
					System.exit(0);
				}
				if(temp_serviceFreq<0)
				{
					System.out.println("Error! serviceFreq must be non negative");
					System.exit(0);
				}
				
				if(temp_threshold<0)
				{
					System.out.println("Error! threshold must be non negative");
					System.exit(0);
				}
				if(temp_noBins<0)
				{
					System.out.println("Error! noBins must be non negative");
					System.exit(0);
				}
				
			}
			catch(Exception ex)
			{
				System.out.println("Error! Incorrect format, describing area"+(i+1));
				System.out.println("Correct order:");
				System.out.println("areaIdx <value> serviceFreq <value> thresholdVal <value> noBins <value>");
				System.exit(0);
			}
			
		
			line=br.readLine();
			if(!line.equalsIgnoreCase("roadsLayout"))
			{
				System.out.println("Error! roadsLayout for area "+temp_areaIdx+" missing");
				System.exit(0);
			}
		
			temp_adjList=new HashMap[temp_noBins+1];
			temp_bins=new LinkedList<Bin>();
			
			for(int j=0;j<temp_noBins+1;j++)
			{
				//Create bin objects for the area. Ignore the depot.
				if(j>0)
					temp_bins.add(new Bin(j,temp_areaIdx,temp_threshold));
				
				//Add map details to adjacency list.
				temp_adjList[j]=new HashMap<Integer,Integer>();
				line=br.readLine();
				
				row=preprocessRow(line.split("\\s"));
				
				for(int k=0;k<temp_noBins+1;k++)
				{
					try
					{
						weight=Integer.parseInt(row[k]);
						if(weight!=-1&&weight>0)
							temp_adjList[j].put(k,weight*60);	//Stores time in seconds.
						else if(weight<0&&weight!=-1)
						{
							System.out.println("Error! Weight must be non-negative in:");
							System.out.println("Area: "+temp_areaIdx+",roadLayout "+line);
							System.exit(0);
						}
							
					}
					catch(Exception ex)
					{
						System.out.println("Error! An error occured while parsing area: "+temp_areaIdx+", roadLayout "+line);
						System.exit(0);
					}
				}
				
			}
			
			currentEnv.addArea(new Area(temp_areaIdx,temp_serviceFreq,temp_threshold,temp_noBins,temp_adjList,temp_bins));
			
		 }
		}
		catch(Exception ex)
		{
			System.out.println("Error! IO exception");
			System.exit(0);
			
		}
	
	}
	
	
	//Generic Parsers
	//***********************************************************************************************************************************************//
	
	//Generic parse for integer values.
	public static int genericIntParser(BufferedReader br,String tokenCaller)
	{
		String temp;
		String line;
		int value=0;
		
		try
		{
			line=br.readLine();
			
			//No value detected.
			if(line.split(" ").length<2)
			{
				System.out.println("Error! No value detected for "+ tokenCaller);
				System.exit(0);
			}
			
			
			//Multiple values detected.
			if(line.split(" ").length>2)
			{
				System.out.println("Warning! Multiple values detected for "+tokenCaller);
				System.out.println("Use the experiment keyword to run experiments");
				System.out.println("Proceeding anyway...");
			}
			
			
			
			
			temp=line.split(" ")[1];
		
			
						
			try
			{
				value=Integer.parseInt(temp);
				
				if(value<0)
				{
					System.out.println("Error! "+tokenCaller+"must be non-negative");
					System.exit(0);
				}
				
				
			}
			catch(Exception ex)
			{
				System.out.println("Error! \'"+temp+"\' is not an integer."+tokenCaller+" must be a positive integer.");
				System.exit(0);
			}
		
		}
		catch(Exception ex)
		{
			System.out.println("Error! An error occurred reading the"+tokenCaller+" token. Formatting might be incorrect.");
			System.exit(0);
		}
		
		return value;
		
	}
	
	//Generic parse for float values.
	public static float genericFloatParser(BufferedReader br,String tokenCaller)
	{
		String temp;
		String line;
		float value=0;
		
		try
		{
			line=br.readLine();
			
			//No value detected.
			if(line.split(" ").length<2)
			{
				System.out.println("Error! No value detected for "+ tokenCaller);
				System.exit(0);
			}
			
			
			//Multiple values detected.
			if(line.split(" ").length>2)
			{
				System.out.println("Warning! Multiple values detected for "+tokenCaller);
				System.out.println("Use the experiment keyword to run experiments");
				System.out.println("Proceeding anyway...");
			}
			
			temp=line.split(" ")[1];
		
			try
			{
				value=Float.parseFloat(temp);
				
				if(value<0)
				{
					System.out.println("Error! "+tokenCaller+"must be non-negative");
					System.exit(0);
				}
				
				
			}
			catch(Exception ex)
			{
				System.out.println("Error! \'"+temp+"\' is not a real valued number."+tokenCaller+" must be a positive real valued number.");
				System.exit(0);
			}
		
		}
		catch(Exception ex)
		{
			System.out.println("Error! An error occurred reading the"+tokenCaller+" token. Formatting might be incorrect.");
			System.exit(0);
		}
		
		return value;
		
	}

	//Generic parse for integer valued experiments.
	public static int[] genericIntParserExp(BufferedReader br,String tokenCaller)
	{
		String[] temp;
		String line;
		int[] values=null;
		int i=0;
		
		try
		{
			line=br.readLine();
			
			//No value detected.
			if(line.split(" ").length<3)
			{
				System.out.println("Error! No values detected for "+ tokenCaller);
				System.exit(0);
			}
			
			temp=new String[line.split(" ").length];
			temp=line.split(" ");
			
			values=new int[temp.length-2];
						
			try
			{
				for(i=2;i<temp.length;i++)
				{
				
					values[i-2]=Integer.parseInt(temp[i]);
				
					if(values[i-2]<0)
					{
						System.out.println("Error! "+tokenCaller+"must be non-negative");
						System.exit(0);
					}
				
				}
			}
			catch(Exception ex)
			{
				System.out.println("Error! \'"+temp[i]+"\' is not an integer."+tokenCaller+" must be a positive integer.");
				System.exit(0);
			}
		
		}
		catch(Exception ex)
		{
			System.out.println("Error! An error occurred reading the"+tokenCaller+" token. Formatting might be incorrect.");
			System.exit(0);
		}
		
		return values;	
	}

	//Generic parse for float valued experiments.
	public static float[] genericFloatParserExp(BufferedReader br,String tokenCaller)
	{
		String[] temp;
		String line;
		float[] values=null;
		int i=0;
		
		try
		{
			line=br.readLine();
			
			//No value detected.
			if(line.split(" ").length<3)
			{
				System.out.println("Error! No values detected for "+ tokenCaller);
				System.exit(0);
			}
			
			temp=new String[line.split(" ").length];
			temp=line.split(" ");
			
			values=new float[temp.length-2];
						
			try
			{
				for(i=2;i<temp.length;i++)
				{
					values[i-2]=Float.parseFloat(temp[i]);
				
					if(values[i-2]<0)
					{
						System.out.println("Error! "+tokenCaller+"must be non-negative");
						System.exit(0);
					}
				
				}
			}
			catch(Exception ex)
			{
				System.out.println("Error! \'"+temp[i]+"\' is not a real valued number."+tokenCaller+" must be a positive real valued number.");
				System.exit(0);
			}
		
		}
		catch(Exception ex)
		{
			System.out.println("Error! An error occurred reading the"+tokenCaller+" token. Formatting might be incorrect.");
			System.exit(0);
		}
		
		return values;	
	}

	
	//Preprocessing and Helper Functions
	//**********************************************************************************************************************************************//
	
	//Calls the function for the relevant token to parse the input file.
	public static void parseIt(String tok,BufferedReader br,SimEnv currentEnv)
		{
			switch(tok)
			{
				case "lorryVolume":
					lorVol(br,currentEnv);
					break;
				case "lorryMaxLoad":
					lorMaxL(br,currentEnv);
					break;
				case "binServiceTime":
					binST(br,currentEnv);
					break;
				case "binVolume":
					binV(br,currentEnv);
					break;
				case "disposalDistrRate":
					dispDR(br,currentEnv);
					break;
				case "disposalDistrShape":
					dispDS(br,currentEnv);
					break;
				case "bagVolume":
					bagV(br,currentEnv);
					break;
				case "bagWeightMin":
					bagWmin(br,currentEnv);
					break;
				case "bagWeightMax":
					bagWmax(br,currentEnv);
					break;
				case "noAreas":
					numAreas(br,currentEnv);
					break;
				case "serviceFreq":
					serviceFreq(br,currentEnv);
					break;
				case "stopTime":
					stopT(br,currentEnv);
					break;
				case "warmUpTime":
					warmUT(br,currentEnv);
					break;
				default: 
					System.out.println("ProgramError: No token "+tok+" defined");
					System.exit(0);
			}
		}
		
	//Returns the row of a matrix without any whitespace.
	public static String[] preprocessRow(String[] original)
	{
		String[] fixed;
		int len=0;		
		for(int i=0;i<original.length;i++)
		{
			if(isNumeric(original[i]))
			{
				len++;
			}
		}
		fixed=new String[len];
		len=0;
		for(int i=0;i<original.length;i++)
		{
			if(isNumeric(original[i]))
			{
				fixed[len]=original[i];
				len++;
			}
		}
		
		
		
		return fixed;
				
				
				
	}

	//Checks that a string is a number.
	public static boolean isNumeric(String str)
	{
		@SuppressWarnings("unused")
		int b;
		try
		{
			b=Integer.parseInt(str);
		}
		catch(Exception ex)
		{
			return false;
		}
		return true;
	}

	//This function checks whether a string is a blank line or a comment.
	public static boolean isBlankComment(String temp)
		{
			if(temp.matches("#(.*)"))
				return true;
			if(temp.replace(" ", "").equals(""))
				return true;
			
			return false;
		}
		
	
}
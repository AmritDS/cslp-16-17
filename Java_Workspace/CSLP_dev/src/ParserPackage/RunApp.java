package ParserPackage;
import java.util.*;
import java.io.*;
public class RunApp
{
	static int expNo;
	
	//Stores whether or not the value is being experimented with.
	static HashMap<String,Boolean> experiment=new HashMap<String,Boolean>();
			
	//Create a file to store statistics in.
	static File stats=new File("Stats.csv");
	
	
	public static void main(String[] args)throws IOException
	{
		File file1=new File(args[0]);
		File file2=new File(args[1]);
		
		//Delete old stats info.
		FileWriter statsout=new FileWriter(stats);
		statsout.write("TripDuration,NoTrips,TripEfficiency,VolumeCollected,BisOverflowed\n"); //All stats collected are overall.
		statsout.close();
		
		//Remove comments and blank lines.
		Parser.removeMetaData(file1,file2);

		
		
		//Parse data and run a sanity check on the parsed values.
		SimEnv currentEnv=Parser.getInfo(file2);
		Parser.sanityCheck(currentEnv);
		
		experiment.put("DisposalDistrRate",false);
		experiment.put("DisposalDistrShape",false);
		experiment.put("ServiceFreq",false);
		
		expNo=0;
		
		//Checks that all areas have connected graphs.
		Setup.checkConnections(currentEnv);
				
		//Create travel sheets for every location in every area.
		Setup.createTravelSheets(currentEnv);
		
		if(currentEnv.isExp())
		{
			Printer.disable();
			runExperiments_forRateShapeFreq(currentEnv);
		}
		else
		{
			Printer.enable();
			runProgram(currentEnv);
			
			System.out.println();
			System.out.println("---");
			Printer.printStats(stats);
		}
		
	}

	public static void runProgram(SimEnv currentEnv)
	{
		//generate the garbage disposal events.
		LinkedList<Event> globalEvents = Setup.setGarbageBags(currentEnv);

		//generate the lorries of the simulation.
		LinkedList<Lorry> lorries = Setup.generateLorries(currentEnv);
		
		//Add the lorry schedules to globalEvents.
		Setup.setLorryShedules(lorries,globalEvents,currentEnv);

		//Reset the statistics.
		Stats.reset(currentEnv);
		
		//Run the simulation generating appropriate output for events.
		RunSimulations.main(globalEvents, currentEnv);
		
	}
	
	public static void runExperiments_forRateShapeFreq(SimEnv currentEnv)
	{
		if(currentEnv.getDisposalDistrRateExp()!=null)
		{
			experiment.put("DisposalDistrRate",true);
			float disposalDistrRate;
			for(int i=0;i<currentEnv.getDisposalDistrRateExp().length;i++)
			{
				disposalDistrRate=currentEnv.getDisposalDistrRateExp()[i];
				currentEnv.setDisposalDistrRate(disposalDistrRate);
				
				runExperiments_forShapeFreq(currentEnv);
			}
		}
		else
		{
			runExperiments_forShapeFreq(currentEnv);
		}
	}

	public static void runExperiments_forShapeFreq(SimEnv currentEnv)
	{
		if(currentEnv.getDisposalDistrShapeExp()!=null)
		{
			experiment.put("DisposalDistrShape",true);
			int disposalDistrShape;
			
			for(int j=0;j<currentEnv.getDisposalDistrShapeExp().length;j++)
			{
				disposalDistrShape=currentEnv.getDisposalDistrShapeExp()[j];
				currentEnv.setDisposalDistrShape(disposalDistrShape);
					
				runExperiments_forFreq(currentEnv);
			}
		}
		else
		{
			runExperiments_forFreq(currentEnv);
		}
	}
	
	public static void runExperiments_forFreq(SimEnv currentEnv)
	{
		if(currentEnv.getServiceFreqExp()!=null)
		{
			experiment.put("ServiceFreq",true);
			float serviceFreq;
			
			for(int k=0;k<currentEnv.getServiceFreqExp().length;k++)
			{
				serviceFreq=currentEnv.getServiceFreqExp()[k];
				for(int a=0;a<currentEnv.getAreas().size();a++)
				{
					currentEnv.getAreas().get(a).setServiceFreq(serviceFreq);
				}
				
				expNo++;
				Printer.printExpNo(expNo,experiment,currentEnv,serviceFreq);
				runProgram(currentEnv);
				Printer.printStats(stats);
			}
		}
		else
		{
			expNo++;
			Printer.printExpNo(expNo,experiment,currentEnv,0);
			runProgram(currentEnv);
			Printer.printStats(stats);
		}
	}
	
}
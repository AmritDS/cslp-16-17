package ParserPackage;
import java.util.*;
public class Location 
{
	int id;
	int[] travelDistances;
	LinkedList<Integer>[] travelPaths;
	
	public Location(int id)
	{
		this.id=id;
		this.travelDistances=null;
		this.travelPaths=null;
	}
	
	public int[] getTravelDistances() {
		return travelDistances;
	}
	public void setTravelDistances(int[] travelDistances) {
		this.travelDistances = travelDistances;
	}
	public LinkedList<Integer>[] getTravelPaths() {
		return travelPaths;
	}
	public void setTravelPaths(LinkedList<Integer>[] travelPaths) {
		this.travelPaths = travelPaths;
	}
	public int getId()
	{
		return id;
	}

	
}

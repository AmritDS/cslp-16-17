package ParserPackage;

import java.util.*;

public class Graph 
{
	static int[] minDist;
	
	//Generates the lorry path according to nearest neighbor heuristics.
	public static LinkedList<Integer> getPathNN(Area area,HashMap<Integer,Boolean> ser)
	{
		
		//Make a copy of the service.
		HashMap<Integer,Boolean> service=new HashMap<Integer,Boolean>();
		Set<Integer> keys=ser.keySet();
		
		for(Integer k :keys)
			service.put(k,ser.get(k));
		
		//All schedules start and end at the depot.
		//Start at the depot and add the closest in the list of bins to be serviced.
		
		//Calculate the number of bins to service.
		int count=0;
		keys=service.keySet();
		
		for(Integer k :keys)
			if(service.get(k))
				count++;
		
		LinkedList<Integer> schedule=new LinkedList<Integer>();
		
		//Start the trip at the depot.
		int initialLocation=0;
		schedule.add(0);
		
		int min=100000000;
		int nextLocation=0;
		int[] travelSheet;
		LinkedList<Integer>[] travelPath;
		
		
		
		travelSheet=area.getDepot().getTravelDistances();
		travelPath=area.getDepot().getTravelPaths();
		
		//Find the closest bin to service.
		for(Integer k :keys)
			if(service.get(k)&&!schedule.contains(k))
			{
				if(travelSheet[k]<min)
				{
					nextLocation=k;
					min=travelSheet[k];
				}
			}
		
		schedule.removeLast();
		schedule.addAll(travelPath[nextLocation]);
		
		//the first bin has been added to the schedule.
		count--;
		service.put(nextLocation,false);
		initialLocation=nextLocation;
		
		//While there still remain bins to service.
		while(count>0)
		{
			min=10000000;
			
			travelSheet=area.getBins().get(initialLocation-1).getTravelDistances();
			travelPath=area.getBins().get(initialLocation-1).getTravelPaths();
			
			keys=service.keySet();
			
			for(Integer k :keys)
				if(service.get(k)&&!schedule.contains(k))
				{
					if(travelSheet[k]<min)
					{
						nextLocation=k;
						min=travelSheet[k];
					}
				}
			
			schedule.removeLast();
			schedule.addAll(travelPath[nextLocation]);

			//the next bin has been added to the schedule.
			count--;
			service.put(nextLocation,false);
			initialLocation=nextLocation;
			
		}
		
		//Add the depot to the schedule and return.
		travelSheet=area.getBins().get(initialLocation-1).getTravelDistances();
		travelPath=area.getBins().get(initialLocation-1).getTravelPaths();
		schedule.removeLast();
		schedule.addAll(travelPath[0]);
		
		
		//Remove the depot from the front of the schedule.
		schedule.removeFirst();
		
		return schedule;
			
	}

	//Generates the lorry path to return to the depot from the currentLocation.
	public static LinkedList<Integer> returnToDepot(int currentLoc, Area area)
	{
		LinkedList<Integer> schedule=new LinkedList<Integer>();

		LinkedList<Integer>[] travelPath=area.getBins().get(currentLoc-1).getTravelPaths();
		schedule.addAll(travelPath[0]);
		schedule.removeFirst(); 	//Do not include currentLoc in the path.
		return schedule;
	}

	//Finds the shortest path from every location to every location and stores the results.
	public static void createTravelSheets(Area area)
	{
		DjikstraAlgorithm(area.getAdjList(),area.getDepot());
		for(int i=0;i<area.getNoBins();i++)
		{
			DjikstraAlgorithm(area.getAdjList(),area.getBins().get(i));
		}
	}

	//Uses Djikstra's algorithm to store the shortest path from the given location to every other location.
	@SuppressWarnings("unchecked")
	private static void DjikstraAlgorithm(HashMap<Integer,Integer>[] graph, Location loc)
	{
		//Number of locations
		int n=graph.length;

		Comparator<Integer> comparator=new WeightComparator();
		PriorityQueue<Integer> unexplored =new PriorityQueue<Integer>(n,comparator);
		minDist=new int[n];
		LinkedList<Integer>[] path=new LinkedList[n];

		for(int i=0;i<n;i++)
		{
			//Set the minimum distance to each location as infinity.
			minDist[i]=100000000;
			path[i]=new LinkedList<Integer>();
			unexplored.add(i);
		} 

		//set the minimum distance to the starting location as 0.
		unexplored.remove(loc.getId());
		minDist[loc.getId()]=0;
		unexplored.add(loc.getId());
		path[loc.getId()].add(loc.getId());

		int currentLoc;
		int neighbor;
		int altDist;

		while(unexplored.size()!=0)
		{
			//Find and remove the closest 
			currentLoc=unexplored.poll();	

			Set<Integer> keys=graph[currentLoc].keySet();
			
			for(Integer k :keys)	//All neighbors of currentLoc.
			{			
				neighbor=k;
				altDist=minDist[currentLoc]+graph[currentLoc].get(neighbor);

				if(altDist<minDist[neighbor])
				{
					unexplored.remove(neighbor);
					minDist[neighbor]=altDist;
					unexplored.add(neighbor);
					
					path[neighbor]=(LinkedList<Integer>)path[currentLoc].clone();
					path[neighbor].add(neighbor);
					
				}
			}
		}

		//Store the gathered information.
		loc.setTravelDistances(minDist);
		loc.setTravelPaths(path);
	}

	//Checks that it is possible to move between any 2 locations in a graph.
	public static void CheckFullyConnected(HashMap<Integer,Integer>[] graph)
	{
		
		
		for(int start=0;start<graph.length;start++)
		{
		
		boolean visited[]=new boolean[graph.length];
		
		LinkedList<Integer> unexplored=new LinkedList<Integer>();
		
		unexplored.add(start);
		int i;
		Set<Integer> keys;
		while(unexplored.size()!=0)
		{
			i=unexplored.getLast();
			unexplored.removeLast();
			visited[i]=true;
			
			keys=graph[i].keySet();
			
			for(int k: keys)
			{
				if(!visited[k])
				{
					unexplored.add(k);
				}
			}
		}
		
		
		for(int j=0;j<visited.length;j++)
			if(!visited[j])
			{
				System.out.println("Error! Location "+j+" unreachable from location "+start);
				System.exit(0);
			}
		}
	}
	
}
	

//Used to aid the priority queue used in DjikstrasAlogrithm
class WeightComparator implements Comparator<Integer>
{

	@Override
	public int compare(Integer x, Integer y)
	{
		if(Graph.minDist[x]<Graph.minDist[y])
			return 1;
		
		if(Graph.minDist[x]>Graph.minDist[y])
			return -1;
		
		return 0;
	}

}
	


package ParserPackage;
import java.util.*;
public class Area 
{
	int areaIdx;
	float serviceFreq;
	float thresholdVal;
	int noBins;
	HashMap<Integer,Integer>[] adjList; //To store the route graph of the area.
	LinkedList<Bin> bins;
	Location depot;
	
	public LinkedList<Bin> getBins() {
		return bins;
	}

	public int getAreaIdx() {
		return areaIdx;
	}

	public void setAreaIdx(int areaIdx) {
		this.areaIdx = areaIdx;
	}

	public float getServiceFreq() {
		return serviceFreq;
	}

	public void setServiceFreq(float serviceFreq) {
		this.serviceFreq = serviceFreq;
	}

	public float getThresholdVal() {
		return thresholdVal;
	}

	public void setThresholdVal(float thresholdVal) {
		this.thresholdVal = thresholdVal;
	}

	public int getNoBins() {
		return noBins;
	}

	public void setNoBins(int noBins) {
		this.noBins = noBins;
	}

	public HashMap<Integer,Integer>[] getAdjList() {
		return adjList;
	}

	
	
	public Location getDepot() {
		return depot;
	}

	Area(int areaIdx,float serviceFreq,float thresholdVal,int noBins,HashMap<Integer,Integer>[] adjList,LinkedList<Bin> bins)
	{
		this.areaIdx=areaIdx;
		this.serviceFreq=serviceFreq;
		this.thresholdVal=thresholdVal;
		this.noBins=noBins;
		this.adjList=adjList;
		this.bins=bins;
		this.depot=new Location(0);
	}
	
	
}

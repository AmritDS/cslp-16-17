package ParserPackage;
import java.util.*;
public class LorryFinishedWorking extends Event
{
	String lorryId;

	public LorryFinishedWorking(String lorryId,long arrivalTime)
	{
		this.lorryId=lorryId;
		super.eventType="Arrive";
		super.eventTime=arrivalTime;
		super.listeners=new LinkedList<EventListener>();
	}

	
}
package ParserPackage;
import java.io.*;
import java.util.HashMap;
import java.util.LinkedList;

public class Printer implements EventListener
{
	static boolean ready=true;

	public static void disable()
	{
		ready=false;
	}
	
	public static void enable()
	{
		ready=true;
	}
	
	public void respondToEvent(Event e,long time)
	{
		if(ready)
		{
			int eventType=0;
			
			//Recognize event.
			if(e.getEventType().equals("GarbageBag"))
				eventType=1;
			
			//Process event.
			switch(eventType)
			{
			case 1:
				printGarbageBag((GarbageBag)e,time);
				break;
			default:
				System.out.println("Internal Error: Could not recognize event");
				System.exit(0);
			}
		}
		
	}
	
	public static void printGarbageBag(GarbageBag gb,long time)
	{
		if(ready)
		{
			System.out.printf(Time.getTFormat(time)+" -> bag weighing %.3f kg disposed of at bin "+gb.getAreaId()+"."+gb.getBinId(),gb.getWeight());
			System.out.println();
		}
		
	}

	public static void printChangeIn_binContents(Bin bin,long time)
	{
		if(ready)
		{
			double volume=bin.getVolume();
			double weight=bin.getWeight();
			int areaId=bin.getAreaId();
			int id=bin.getId();
			
			System.out.printf(Time.getTFormat(time)+" -> load of bin " + areaId +"." +id+" became %.3f",weight);
			System.out.printf(" and contents volume %.3f m^3",volume);
			System.out.println();
		}
	}

	public static void printThresholdExceeded(Bin bin,long time)
	{
		if(ready)
		{
			int areaId=bin.getAreaId();
			int id=bin.getId();
			System.out.println(Time.getTFormat(time)+" -> occupancy threshold of bin " + areaId +"." +id+" exceeded.");
		}

	}
	
	public static void printBinOverflow(Bin bin,long time)
	{	
		if(ready)
		{
			int areaId=bin.getAreaId();
			int id=bin.getId();
			System.out.println(Time.getTFormat(time)+" -> bin " + areaId +"." +id+" overflowed");
		}
	}
	
	public static void printChangeIn_lorryContents(Lorry lorry,long time)
	{
		if(ready)
		{
			double volume=lorry.getLorryVolume();
			double weight=lorry.getLorryLoad();
			int areaId=lorry.getArea().getAreaIdx();
			
			System.out.printf(Time.getTFormat(time)+" -> load of lorry " + areaId +" became %.3f",weight);
			System.out.printf(" and contents volume %.3f m^3",volume);
			System.out.println();
		}
	}

	public static void printLorryArrived(Lorry lorry,long time)
	{
		if(ready)
		{
			int areaId=lorry.getArea().getAreaIdx();
			int location=lorry.getLocation();
			
			System.out.printf(Time.getTFormat(time)+" -> lorry " + areaId+" arrived at location "+areaId+"."+location);
			System.out.println();
		}
	}
	
	public static void printLorryLeft(Lorry lorry,long time)
	{
		if(ready)
		{
			int areaId=lorry.getArea().getAreaIdx();
			int location=lorry.getLocation();
			
			System.out.printf(Time.getTFormat(time)+" -> lorry " + areaId+" left location "+areaId+"."+location);
			System.out.println();
		}
	}
	
	public static void printExpNo(int expNo,HashMap<String,Boolean> experiment,SimEnv currentEnv,double serviceFreq)
	{
		System.out.print("Experiment #"+expNo+" ");
		
		if(experiment.get("DisposalDistrRate"))
		{
			System.out.print("disposalDistrRate "+currentEnv.getDisposalDistrRate()+" ");
		}
		
		if(experiment.get("DisposalDistrShape"))
		{
			System.out.print("disposalDistrShape "+currentEnv.getDisposalDistrShape()+" ");
		}
		
		if(experiment.get("ServiceFreq"))
		{
			System.out.print("serviceFreq "+serviceFreq+" ");
		}
		
		System.out.println();
		System.out.println("---");
	}

	//Print statistics to screen and also to file.
	public static void printStats(File file)
	{
		try{
		LinkedList<Integer> keys=Stats.getKeys();
		HashMap<Integer,AreaStats> areaStats=Stats.getAreaStats();
		
		FileWriter fout=new FileWriter(file,true);
		
		AreaStats area;
		float sumAvgTripDuration=0;
		float sumAvgTripsperShedule=0;
		float sumTripEfficiency=0;
		float sumAvgVolumeCollected=0;
		float sumPercentageOverflowedBins=0;
		
		for(int i=0;i<keys.size();i++)
		{
			area=areaStats.get(keys.get(i));
			System.out.print("area "+area.getAreaId()+": ");
			System.out.println("average trip duration "+Time.getTFormat(area.getTravelTime()/area.getNoOfTrips()));
			sumAvgTripDuration+=area.getTravelTime()/area.getNoOfTrips();
			
		}
		System.out.println("overall average trip duration: "+Time.getTFormat((long)sumAvgTripDuration/keys.size()));
		fout.write((long)sumAvgTripDuration/keys.size()+",");	//Store time in seconds.
			
		for(int i=0;i<keys.size();i++)
		{
			area=areaStats.get(keys.get(i));
			System.out.print("area "+area.getAreaId()+": ");
			System.out.println("average no. trips "+(float)area.getNoOfTrips()/area.getNoOfShedules());
			sumAvgTripsperShedule+=(float)area.getNoOfTrips()/area.getNoOfShedules();
			
		}
		System.out.println("overall average no. trips "+(float)sumAvgTripsperShedule/keys.size());
		fout.write((float)sumAvgTripsperShedule/keys.size()+",");
		
		for(int i=0;i<keys.size();i++)
		{
			area=areaStats.get(keys.get(i));
			System.out.print("area "+area.getAreaId()+": ");
			System.out.printf("trip Efficiency %.3f \n",(float)60*area.getWeightCollected()/area.getTravelTime());
			sumTripEfficiency+=(float)60*area.getWeightCollected()/area.getTravelTime();
		}
		System.out.printf("overall trip efficiency %.3f \n",sumTripEfficiency/keys.size());
		fout.write(String.format("%.3f",sumTripEfficiency/keys.size())+",");
		
		for(int i=0;i<keys.size();i++)
		{
			area=areaStats.get(keys.get(i));
			System.out.print("area "+area.getAreaId()+": ");
			System.out.printf("average volume collected %.3f \n",(float)area.getVolumeCollected()/area.getNoOfTrips());
			sumAvgVolumeCollected+=(float)area.getVolumeCollected()/area.getNoOfTrips();
			
		}
		System.out.printf("overall average volume collected %.3f \n",sumAvgVolumeCollected/keys.size());
		fout.write(String.format("%.3f",sumAvgVolumeCollected/keys.size())+",");
		
		for(int i=0;i<keys.size();i++)
		{
			area=areaStats.get(keys.get(i));
			System.out.print("area "+area.getAreaId()+": ");
			System.out.printf("percentage of bins overflowed %.3f \n",(float)100*area.getTotNoOverflow()/(area.getNoOfShedules()*area.getNoBins()));
			sumPercentageOverflowedBins+=(float)100*area.getTotNoOverflow()/(area.getNoOfShedules()*area.getNoBins());

		}
		System.out.printf("overall percentage of bins overflowed %.3f \n",sumPercentageOverflowedBins/keys.size());
		fout.write(String.format("%.3f",sumPercentageOverflowedBins/keys.size())+"\n");
		
		System.out.println("---");
		
		fout.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(0);
		}
	}

}

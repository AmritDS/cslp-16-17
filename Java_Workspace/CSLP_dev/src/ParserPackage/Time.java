package ParserPackage;

public class Time 
{
	public static String getTFormat(long time)
	{
		int days=0;
		int hours=(int)time/3600;
		int miniutes=(int)(time/60-hours*60);
		int seconds=(int)(time-miniutes*60-hours*3600);
		
		
		if(hours>24)
		{
			days=(int)hours/24;
			hours=hours-days*24;
		}
		
		String d=String.format("%02d",days);
		String h=String.format("%02d",hours);
		String m=String.format("%02d",miniutes);
		String s=String.format("%02d",seconds);
		
		return (d+":"+h+":"+m+":"+s);
		
		
	}
}

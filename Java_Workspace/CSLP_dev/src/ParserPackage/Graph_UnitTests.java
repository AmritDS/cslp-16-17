package ParserPackage;

import java.util.*;
import java.io.*;


public class Graph_UnitTests 
{
	static SimEnv currentEnv;
	
	//Parse the UnitTest file and create travel sheets.
	public static void main(String[] args)throws IOException
	{
		File UnitTestFileRaw =new File("UnitTestInputRaw.txt");
		File UnitTestFile=new File("UnitTestInput.txt");
		Parser.removeMetaData(UnitTestFileRaw,UnitTestFile);
		
		currentEnv=Parser.getInfo(UnitTestFile);
		//Create travel sheets for every location in every area.
		Setup.createTravelSheets(currentEnv);
		
		getPathNN_Test();
		returnToDepot_Test();
		
		
	}
	
	//Check that the expected path and computed path are the same.
	public static void getPathNN_Test()
	{
		LinkedList<Integer> path;
		LinkedList<Integer> expectedPath=new LinkedList<Integer>();
		HashMap<Integer,Boolean> service=new HashMap<Integer,Boolean>();
		int id;
		
		//Set the expected path.
		for(int i=0;i<currentEnv.getAreas().get(0).getNoBins();i++)
		{
			id=currentEnv.getAreas().get(0).getBins().get(i).getId();
			
			expectedPath.add(id);
		}
		for(int i=currentEnv.getAreas().get(0).getNoBins()-2;i>=0;i--)
		{
			id=currentEnv.getAreas().get(0).getBins().get(i).getId();
			
			expectedPath.add(id);
		}
		expectedPath.add(0);
		
		//Set alternate bins to be serviced.
		for(int i=0;i<currentEnv.getAreas().get(0).getNoBins();i++)
		{
			id=currentEnv.getAreas().get(0).getBins().get(i).getId();
			
			if(i%2==0)
			{	
				service.put(id,true);
			}
			else
				service.put(id,false);
		}
		
		
		path=Graph.getPathNN(currentEnv.getAreas().get(0),service);
		
		//Check if the expected path was returned.		
		for(int i=0;i<path.size();i++)
			if(!(path.get(i)==expectedPath.get(i)))
			{
				System.out.println("Error! Expected Path and Actual Path do not match.");
				System.out.println("Error in nearest neighbor heuristics");
			}
		
		
	}

	//Checks the path created by the returnToDepot function ends at the depot.
	public static void returnToDepot_Test()
	{
		if(!(Graph.returnToDepot(5,currentEnv.getAreas().get(0)).getLast()==0))
		{
			System.out.println("Error! Return to depot schedule does not end at the Depot.");
		}
	}
	

}

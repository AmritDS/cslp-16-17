package ParserPackage;

import java.util.LinkedList;

public class binEmptied extends Event
{	
	Bin bin;
	
	public binEmptied(Bin bin,long time)
	{
		this.bin=bin;
		super.eventTime=time;
		super.listeners=new LinkedList<EventListener>();
		super.setEventType("BinEmptied");
	}
	
	public Bin getBin()
	{
		return bin;
	}

		
}

package ParserPackage;

public class Bin extends Location implements EventListener
{
	int areaId;
	double threshold;
	double maxVolume;
	double weight;
	double volume;
	
	boolean underThreshold;
	boolean overflowed;
	
	public Bin(int id, int areaId,double threshold)
	{
		super(id);
		underThreshold=true;
		overflowed=false;
		this.weight=0;
		this.volume=0;
		this.areaId=areaId;
		this.threshold=threshold*maxVolume;
		

	}
	
	
	public void respondToEvent(Event e, long time)
	{
		//Check if the bin is has space, else do nothing. i.e discard the garbage disposal.
		if(!overflowed)
		{
			GarbageBag gb=null;
			//Cast to garbage bag
			if(e.getEventType().equals("GarbageBag"))
				gb=(GarbageBag)e;
			else
			{
				System.out.println("Internal Error: Could not cast event to GarbageBag");
				System.exit(0);
			}
		
			//Add weight
			weight=weight+gb.getWeight();
			
			//Bin volume changes to something under threshold.
			if(volume+gb.volume<threshold)
			{
				volume=volume+gb.volume;
				Printer.printChangeIn_binContents(this, time);
			}
			//Bin volume changes to something between threshold and maxVolume.
			else if(volume+gb.getVolume()>threshold&&volume+gb.getVolume()<maxVolume)
			{
				volume=volume+gb.volume;
				Printer.printChangeIn_binContents(this, time);
				
				if(underThreshold)
				{
					Printer.printThresholdExceeded(this,time);
					underThreshold=false;
				}
				
			}
			
			//Bin volume changes and the bin overflows.
			else if(volume+gb.getVolume()>maxVolume)
			{
				volume=maxVolume;
				overflowed=true;
				
				Printer.printChangeIn_binContents(this, time);
				Printer.printBinOverflow(this, time);
				
				//Report the bin overflow to statistics.
				Stats.modifyStats(areaId).addOverflowedBin(time);
				
			}
		
		
		}
		
	}


	public void setMaxVolume(double maxVolume)
	{
		this.maxVolume=maxVolume;
	}
	
	public int getAreaId() 
	{
		return areaId;
	}



	public double getWeight()
	{
		return weight;
	}



	public double getVolume() 
	{
		return volume;
	}


	public boolean underThreshold()
	{
		return underThreshold;
	}
	
	public void service()
	{
		underThreshold=true;
		overflowed=false;
		this.weight=0;
		this.volume=0;
	}
}

package ParserPackage;

import java.util.LinkedList;

public class RunSimulations 
{
	//Runs the mains simulation.
	public static void main(LinkedList<Event> globalEvents,SimEnv currentEnv)
	{
		long time=globalEvents.get(0).getEventTime();
		
				
		while(time<currentEnv.getStopTime()&&globalEvents.size()!=0)
		{
			//Time of next event.
			time=globalEvents.get(0).getEventTime();
			//Execute Event.
			globalEvents.get(0).execute(time);	
			globalEvents.remove(0);
		}
		
	}
	
}

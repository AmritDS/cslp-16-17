package ParserPackage;
import java.util.*;
public class LorrySheduleEvent extends Event
{

	LinkedList<Event> globalEvents;

	public LorrySheduleEvent(long eventTime,LinkedList<Event> globalEvents)
	{
		this.globalEvents=globalEvents;
		super.eventTime=eventTime;
		super.listeners=new LinkedList<EventListener>();
		super.setEventType("LorrySheduleEvent");
	}


	public LinkedList<Event> getGlobalEvents()
	{
		return globalEvents;
	}


}

package ParserPackage;

import java.util.LinkedList;

public class GarbageBag extends Event
{
	double weight;
	double volume;
	int binId;
	int areaId;
		
	public GarbageBag(float volume,float bagWeightMin,float bagWeightMax,int binId,int areaId,long disposalTime)
	{
		
		this.volume=volume;
		this.weight=bagWeightMin+Math.random()*(bagWeightMax-bagWeightMin);		
		this.binId=binId;
		this.areaId=areaId;
		super.eventTime=disposalTime;
		super.listeners=new LinkedList<EventListener>();
		super.setEventType("GarbageBag");
	}
	
	//Getter methods.
	/////////////////////////////////////////////////////////////////////////////////////
	public double getVolume() 
	{
		return volume;
	}

	public double getWeight() 
	{
		return weight;
	}

	public int getBinId() 
	{
		return binId;
	}

	public int getAreaId() 
	{
		return areaId;
	}
	
	

}

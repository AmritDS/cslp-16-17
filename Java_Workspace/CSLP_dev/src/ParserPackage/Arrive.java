package ParserPackage;
import java.util.*;
public class Arrive extends Event
{
	int lorryId;
	int location;
	LinkedList<Event> globalEvents;

	public Arrive(int lorryId,int location,long arrivalTime,LinkedList<Event> globalEvents)
	{
		this.lorryId=lorryId;
		this.location=location;
		this.globalEvents=globalEvents;
		super.eventType="Arrive";
		super.eventTime=arrivalTime;
		super.listeners=new LinkedList<EventListener>();

	}

	public int getLocation()
	{
		return location;
	}
	
	public LinkedList<Event> getGlobalEvents()
	{
		return globalEvents;
	}

}

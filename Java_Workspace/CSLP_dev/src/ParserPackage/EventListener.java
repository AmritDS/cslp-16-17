package ParserPackage;

public interface EventListener 
{
	void respondToEvent(Event e,long time);
}

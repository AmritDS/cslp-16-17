package ParserPackage;
import java.util.*;
public class Lorry implements EventListener
{

	int lorry_Max_Volume;
	int lorry_Max_Load;
	float serviceFreq;
	int binServiceTime;

	double lorryVolume;
	double lorryLoad;

	//this is true if the lorry had to return to the depot during a schedule due to capacity constraints.
	boolean sheduleInterrupted;
	LinkedList<Event> tempGlobalEvents;	//Holds a pointer to the globalEvents list that is required incase the schedule is interrupted.										

	Area area;
	//state will be 0 if at the depot and not busy. -1 otherwise.
	int state; 

	//Lorry location
	int location;
	
	//Stores the time the trip started.
	long start_Trip;

	//The schedule is a list of bins to visit.
	LinkedList<Integer> shedule;

	//Decide whether or not to service the bin.
	HashMap<Integer,Boolean> service;

	//List of pending events.
	LinkedList<LorrySheduleEvent> pendingEvents;

	public Lorry(Area area,int lorry_Max_Volume,int lorry_Max_Load, float serviceFreq,int binServiceTime)
	{
		this.state=0;	//at the depot, not busy.
		this.location=0;
		
		this.sheduleInterrupted=false;
		
		this.lorryVolume=0;
		this.lorryLoad=0;
		
		this.area=area;
		this.lorry_Max_Volume=lorry_Max_Volume;
		this.lorry_Max_Load=lorry_Max_Load;
		this.serviceFreq=serviceFreq;
		this.binServiceTime=binServiceTime;
		
		
		this.shedule=new LinkedList<Integer>(); 				//No items on the current schedule.
		
		this.service=new HashMap<Integer,Boolean>();
		for(int i=0;i<area.getNoBins();i++)
			service.put(area.getBins().get(i).getId(),false);	//Set all bins to no service required.
		
		
		this.pendingEvents=new LinkedList<LorrySheduleEvent>();	//No pending schedules.
		
	}


	public float getServiceFreq()
	{
		return serviceFreq;
	}

	public void respondToEvent(Event e,long time)
	{
		//lorrySE has access to the globalEvents list.

		int eventNumber=0;
		
		LorrySheduleEvent lorrySE=null;
		Arrive arrivalEvent=null;
		lorryEmptied emptiedEvent=null;
		binEmptied binEmpty=null;
		
		if(e.getEventType().equals("LorrySheduleEvent"))
			eventNumber=1;
		else if(e.getEventType().equals("Arrive"))
			eventNumber=2;
		else if(e.getEventType().equals("LorryEmptied"))
			eventNumber=3;
		else if(e.getEventType().equals("BinEmptied"))
			eventNumber=4;
	

		switch(eventNumber)
		{
			case 1:
				lorrySE=(LorrySheduleEvent)e;	//cast to event type

				if(state==0) //Lorry at the depot, not busy.
				{
					startLorryTrip(lorrySE.getEventTime(),lorrySE.getGlobalEvents());
				}
				else //Lorry in the middle of a schedule.
				{
					//The lorry will respond to the schedule after it completes the current schedule.
					pendingEvents.add(lorrySE);
				}
				
				break;

			case 2:
				arrivalEvent=(Arrive)e;	//cast to event type
				if(arrivalEvent.getLocation()==shedule.get(0))
				{
					shedule.remove(0); //the lorry has arrived so the location can be removed from the schedule.
					location=arrivalEvent.getLocation();
				}
				else
				{
					System.out.println("Lorry "+area.getAreaIdx()+" arrived at unexpected location.");
					System.out.println("Expected location "+shedule.get(0));
					System.out.println("Lorry arrived at "+arrivalEvent.getLocation());
					System.exit(0);
				}
				
				//Print arrival.
				Printer.printLorryArrived(this,arrivalEvent.getEventTime());
				
				//set true if the lorry capacity is not sufficient.
				boolean returnToDepot=false;
				
				//set true if the lorry is emptying its contents.
				boolean lorryEmptying=false;
				
				if(arrivalEvent.getLocation()==0)	//Lorry at depot
				{
					//If the lorry has returned because the lorry was full or because it's schedule was finished then empty it.
					//Else do nothing and set the next arrival.
					if(lorryVolume>=lorry_Max_Volume||lorryLoad>=lorry_Max_Load||shedule.size()==0)
					{
						
						//Empty lorry contents.
						lorryEmptying=true;
						
						long emptiedTime=arrivalEvent.getEventTime()+(long)(5*binServiceTime);
						//create a lorryEmptied event.
						lorryEmptied empty=new lorryEmptied(emptiedTime);
						
						//set listener printer and this lorry.
						empty.addListener(this);

						//add to globalEvents.
						Setup.insert(empty,arrivalEvent.getGlobalEvents());
					}
				}
				else //Lorry arrived at a bin
				{
					//get binId
					int binId=arrivalEvent.getLocation();
					
					//Check if the bin is on the list to be serviced.
					if(service.get(binId))
					{		//Check if the lorry has enough volume capacity.
							if(lorry_Max_Volume-lorryVolume>area.getBins().get(binId-1).getVolume()*2)	//Accounting for the fact that the lorry compresses garbage.
							{
								//Check if the lorry has enough load capacity.
								if(lorry_Max_Load-lorryLoad>area.getBins().get(binId-1).getWeight())
								{
									
									//Empty the bin.
									long emptiedTime=arrivalEvent.getEventTime()+binServiceTime;
									//create a binEmptied event.
									binEmptied empty=new binEmptied(area.getBins().get(binId-1),emptiedTime);
									
									//set listener printer and this lorry.
									empty.addListener(this);

									//add to globalEvents.
									Setup.insert(empty,arrivalEvent.getGlobalEvents());
									
								}
								else
								{
									//Not enough load capacity.									
									//set a flag to return to the depot.
									returnToDepot=true;
								}
							}
							else
							{
								//Not enough volume capacity.
								//set a flag to return to the depot.
								returnToDepot=true;
							}
							
					}
					
				}
				
				//Do nothing if the trip is completed (i.e lorry emptying contents).
				if(lorryEmptying)
				{
					break;
				}
				
				//Schedule not completed.
				//set the next arrival event.
					Arrive nextArrival;
					long arrivalTime;
					
					if(returnToDepot)
					{
						//Schedule will be recomputed to return to the depot.
						shedule=Graph.returnToDepot(arrivalEvent.getLocation(),area);
						
						//Set the service for each bin to be false. 
						//(So that the lorry does not try to service bins on the way back.)
						for(int i=0;i<area.getNoBins();i++)
							service.put(area.getBins().get(i).getId(),false);
						
						
						arrivalTime=area.getAdjList()[location].get(shedule.get(0)) +	//Time taken to move between location and the first stop on the schedule. 
									arrivalEvent.getEventTime(); 
						
						sheduleInterrupted=true;
						tempGlobalEvents=arrivalEvent.getGlobalEvents();


					}
					else
					{
						arrivalTime=area.getAdjList()[location].get(shedule.get(0)) +	//Time taken to move between location and the first stop on the schedule. 
									arrivalEvent.getEventTime() +
									binServiceTime;
					}	
					
					//Create an arrive event for the next location the lorry must visit.
					nextArrival=new Arrive(area.getAreaIdx(),shedule.get(0),arrivalTime,arrivalEvent.getGlobalEvents());		
						
					//Set this lorry as a listener to the arrive event.
					nextArrival.addListener(this);
						
					//Add this event to the list of globalEvents.
					Setup.insert(nextArrival,arrivalEvent.getGlobalEvents());
				
				break;

			case 3:
				emptiedEvent=(lorryEmptied)e;
				
				//Report the end of the trip to statistics.
				Stats.modifyStats(area.getAreaIdx()).addToTravelTime(start_Trip,emptiedEvent.getEventTime());
				Stats.modifyStats(area.getAreaIdx()).addVolumeCollected(lorryVolume,start_Trip);
				Stats.modifyStats(area.getAreaIdx()).addWeightCollected(lorryLoad,start_Trip);
				//Empty the lorry.
				lorryVolume=0;
				lorryLoad=0;
				
				//Print the change in contents.
				Printer.printChangeIn_lorryContents(this,emptiedEvent.getEventTime());
				
				
				
				//Need to check for unfinished schedules and pending trips.
				if(sheduleInterrupted)
				{
					startLorryTrip(emptiedEvent.getEventTime(),tempGlobalEvents); //globalEvents is just a pointer, it doesn't matter where we get it from. We just need access.
					sheduleInterrupted=false;
				}
				else
				{
					//Report the end of the schedule to statistics.
					Stats.modifyStats(area.getAreaIdx()).addShedule(start_Trip);
					
					if(pendingEvents.size()!=0)//Start the next schedule if pending.
					{
						startLorryTrip(emptiedEvent.getEventTime(),pendingEvents.get(0).getGlobalEvents());
						pendingEvents.remove(0);
					}
					else
					{
						state=0;
					}
				}
				
				break;
			
			case 4:
				binEmpty=(binEmptied)e;
				Bin bin=binEmpty.getBin();
				
				//Transfer contents of the bin to the lorry.
				lorryVolume=lorryVolume+bin.getVolume()/2;	//The lorry compresses the garbage.			
				lorryLoad=lorryLoad+bin.getWeight();
				bin.service();
				
				//Print the change in contents.
				Printer.printChangeIn_binContents(bin,binEmpty.getEventTime());
				Printer.printChangeIn_lorryContents(this, binEmpty.getEventTime());
				
				//Print lorry departure.
				Printer.printLorryLeft(this, time);
				
				break;
				
			default:
				System.out.println("Internal Error: Could not recognize event.");
				System.exit(0);
		}

	}

	public int startLorryTrip(long time,LinkedList<Event> globalEvents)
	{
		
		//set the time that the trip started.
		start_Trip=time;
		//Mark bins over threshold.
		for(int i=0;i<area.getNoBins();i++)
		{
			if(!area.getBins().get(i).underThreshold())
				service.put(area.getBins().get(i).getId(),true);
			else
				service.put(area.getBins().get(i).getId(),false);
		
		}			
		
		//Store the path to travel for the schedule (ending at the depot).
		shedule=Graph.getPathNN(area,service);
		
		//If there are no bins to service, then end the function.
		if(shedule.size()==0)
		{
			System.out.println("No bins to service");
			return 0;
		}
	
		//Arrival time is the current time+ time required to get from the first location to the next location.
		long arrivalTime=area.getAdjList()[0].get(shedule.get(0)) +	//Time taken to move between depot and the first stop on the schedule. 
						 time; 
		//Create an arrive event for the first location the lorry must visit.
		Arrive first=new Arrive(area.getAreaIdx(),shedule.get(0),arrivalTime,globalEvents);		
	
		//Set this lorry as a listener to the arrive event.
		first.addListener(this);
	
		//Add this event to the list of globalEvents.
		Setup.insert(first,globalEvents);

		//Print lorry departure.
		Printer.printLorryLeft(this, time);
		
		//set lorry state to -1 as the schedule has begun.
		state=-1;

		return -1;
	}

	public double getLorryVolume() {
		return lorryVolume;
	}


	public double getLorryLoad() {
		return lorryLoad;
	}


	public Area getArea() {
		return area;
	}


	public int getLocation() {
		return location;
	}
	
	
}
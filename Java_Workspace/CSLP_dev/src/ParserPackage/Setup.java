package ParserPackage;
import java.util.*;
public class Setup
{

	//Add the lorry schedules to globalEvents
	public static void setLorryShedules(LinkedList<Lorry> lorries,LinkedList<Event> globalEvents,SimEnv currentEnv)
	{
		LinkedList<Event> lorryShedules=new LinkedList<Event>();
		LinkedList<Event> singleShedules;
		LorrySheduleEvent temp;

		long timeInterval;
		long time;

		for(int i=0;i<lorries.size();i++)
		{
			
			singleShedules=new LinkedList<Event>();

			timeInterval=(long) (3600/lorries.get(i).getServiceFreq());

			//No point starting lorry schedules before the simulation disposes of garbage bags.
			time=timeInterval;
			
			while(time<currentEnv.getStopTime())
				{
					temp=new LorrySheduleEvent(time,globalEvents);
					
					//set the lorry as a listener to the LorrySheduleEvent.
					temp.addListener(lorries.get(i));
					
					singleShedules.add(temp);
					time=time+timeInterval;
				}

			mergeLists(lorryShedules,singleShedules);
		}


		//Include the lorrySheduleEvents in the globalEvents list.
		mergeLists(globalEvents,lorryShedules);

	}

	//generate lorries
	public static LinkedList<Lorry>generateLorries(SimEnv currentEnv)
	{
		LinkedList<Area> areas=currentEnv.getAreas();
		LinkedList<Lorry> lorries=new LinkedList<Lorry>();
		int lorryVolume=currentEnv.getLorryVolume();
		int lorryMaxLoad=currentEnv.getLorryMaxLoad();
		int binServiceTime=currentEnv.getBinServiceTime();
		float serviceFreq;

		//create one lorry for each area.
		for(int i=0;i<areas.size();i++)
		{
			serviceFreq=areas.get(i).getServiceFreq();
			//Note all lorrys are initialized with state 0, i.e not busy, at the depot.
			lorries.add(new Lorry(areas.get(i),lorryVolume,lorryMaxLoad,serviceFreq,binServiceTime));
		}

		return lorries;
	}


	//Simulates disposal of garbage bags and returns a list of events.
	public static LinkedList<Event>setGarbageBags(SimEnv currentEnv)
	{
		
		Area area;
		LinkedList<Event> binEvents;
		LinkedList<Event> areaEvents;
		LinkedList<Event> globalEvents=new LinkedList<Event>();
		long time=0;
		Event tempGB;
		Printer printer=new Printer();
		
		for(int i=0;i<currentEnv.getAreas().size();i++)
		{
			area=currentEnv.getAreas().get(i);
			areaEvents=new LinkedList<Event>();
			
			//set areaEvents to null.
			for(int j=0;j<area.getNoBins();j++)
			{
				//Run simulation for time.
				
				time=0;
				binEvents=new LinkedList<Event>();
				
				//generate new waiting time and add this to time.
				double rateSeconds=currentEnv.getDisposalDistrRate()/3600;
				int shape=currentEnv.getDisposalDistrShape();
				time=time+ Math.round(-sumLogURV(shape)/rateSeconds);
				
				while(time<currentEnv.getStopTime())
				{
					//create a new Event object
					tempGB=new GarbageBag(currentEnv.getBagVolume(),currentEnv.getBagWeightMin(),currentEnv.getBagWeightMax(),area.getBins().get(j).getId(),area.getAreaIdx(),time);
					
					//Add event listeners to the Event.
					tempGB.addListener(printer);
					tempGB.addListener(area.getBins().get(j));
					
					//Add it to binEvents list.(Keep it ordered.)
					binEvents.add(tempGB);
					
					//generate new waiting time and add this to time.
					rateSeconds=currentEnv.getDisposalDistrRate()/3600;
					shape=currentEnv.getDisposalDistrShape();
					time=time+ Math.round(-sumLogURV(shape)/rateSeconds);
				}
						
				
				//Merge binEvents to areaEvents(Keeping it ordered.)
				mergeLists(areaEvents,binEvents);
				//System.out.println(binEvents.size()+" "+areaEvents.size());
			}
			
			//Merge areaEvents to globalEvents.(Keeping it ordered.)
			mergeLists(globalEvents,areaEvents);
			
		}
		
		return globalEvents;
		
		
	}
	
	//Returns the product of n uniform random variables.
	public static double sumLogURV(int n)
	{
			double sum=0;
			for(int i=0;i<n;i++)
				sum=sum+Math.log(Math.random());
			
			return sum;
	}
		
	//Merges two lists of events (Assuming both lists are in ascending order).
	public static void mergeLists(LinkedList<Event> result, LinkedList<Event> B)
	{
			LinkedList<Event> A=new LinkedList<Event>();
			
			while(result.size()>0)
			{
				A.add(result.get(0));
				result.remove(0);
			}
			int i=0,j=0;
			while(i<A.size()&&j<B.size())
			{
				if(A.get(i).getEventTime()<B.get(j).getEventTime())
				{
					result.add(A.get(i));
					i++;
				}
				else
				{
					result.add(B.get(j));
					j++;
				}
			}
			
			while(i<A.size())
			{
				result.add(A.get(i));
				i++;
			}
			
			while(j<B.size())
			{
				result.add(B.get(j));
				j++;
			}
			
		}

	//Inserts an event into the globalEvents list.
	public static void insert(Event event,LinkedList<Event> globalEvents)
	{
		for(int i=0;i<globalEvents.size();i++)
			if(globalEvents.get(i).getEventTime()>event.getEventTime())
			{
				globalEvents.add(i,event);
				break;
			}
		
	}
	
	//Create travel sheets for each location in each area.
	public static void createTravelSheets(SimEnv currentEnv)
	{
		for(int i=0;i<currentEnv.getAreas().size();i++)
		{
			Graph.createTravelSheets(currentEnv.getAreas().get(i));
		}
	}

	//Checks that all areas have connected graphs.
	public static void checkConnections(SimEnv currentEnv)
	{
		for(int i=0;i<currentEnv.getAreas().size();i++)
		{
			Graph.CheckFullyConnected(currentEnv.getAreas().get(i).getAdjList());
		}
	}
}
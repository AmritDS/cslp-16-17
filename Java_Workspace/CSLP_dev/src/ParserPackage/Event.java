package ParserPackage;

import java.util.LinkedList;

public class Event 
{
	//Event type
	protected String eventType;
	
	//Event time in seconds
	protected long eventTime;
	
	//List of Event Listeners.
	protected LinkedList<EventListener> listeners;

	//Call all event listeners associated with this event.
	public void execute(long time)
	{
		for(EventListener e : listeners)
		{
			e.respondToEvent(this,time);
		}
	}
	
	//Add an event listener.
	public void addListener(EventListener toAdd)
	{
			listeners.add(toAdd);
	}
	
	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	
	public long getEventTime()
	{
		return eventTime;
	}
}

package ParserPackage;
import java.util.*;
public class Stats 
{
	public static HashMap<Integer,AreaStats> areaStats;
	public static LinkedList<Integer> keys;
	public static long warmUpTime;
	public static void reset(SimEnv currentEnv)
	{
		areaStats=new HashMap<Integer,AreaStats>();
		keys= new LinkedList<Integer>();
		warmUpTime=currentEnv.getWarmUpTime();
		AreaStats newAreaStat;
		int areaId;
		int noBins;
		
		for(int i=0;i<currentEnv.getAreas().size();i++)
		{
			areaId=currentEnv.getAreas().get(i).getAreaIdx();
			noBins=currentEnv.getAreas().get(i).getNoBins();
			
			newAreaStat=new AreaStats(areaId,noBins,warmUpTime);
			areaStats.put(areaId, newAreaStat);
			keys.add(areaId);
		}
	}
	
	public static AreaStats modifyStats(int areaId)
	{
		return areaStats.get(areaId);
	}

	public static HashMap<Integer, AreaStats> getAreaStats() {
		return areaStats;
	}

	public static LinkedList<Integer> getKeys() {
		return keys;
	}
	
	
	
}

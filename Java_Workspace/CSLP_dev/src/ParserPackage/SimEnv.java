package ParserPackage;
import java.util.*;
public class SimEnv
{
	int lorryVolume;
	int lorryMaxLoad;
	int binServiceTime;
	float binVolume;
	
	float disposalDistrRate;	
	float[] disposalDistrRateExp;	//Ignore if null
	
	int disposalDistrShape;
	int[] disposalDistrShapeExp;	//Ignore if null
	
	float[] serviceFreqExp;			//Ignore if null
	
	boolean isExp;	//true if there are values to experiment with.
	
	float bagVolume;
	float bagWeightMin;
	float bagWeightMax;
	int noAreas;
	long stopTime;
	long warmUpTime;
	LinkedList<Area> areas; 
	
	public boolean isExp() 
	{
		return isExp;
	}


	public void setExp(boolean isExp) 
	{
		this.isExp = isExp;
	}
	
	public float[] getDisposalDistrRateExp() 
	{
		return disposalDistrRateExp;
	}


	public void setDisposalDistrRateExp(float[] disposalDistrRateExp) {
		this.disposalDistrRateExp = disposalDistrRateExp;
		isExp=true;
	}


	public int[] getDisposalDistrShapeExp() {
		return disposalDistrShapeExp;
	}


	public void setDisposalDistrShapeExp(int[] disposalDistrShapeExp) {
		this.disposalDistrShapeExp = disposalDistrShapeExp;
		isExp=true;
	}


	public float[] getServiceFreqExp() {
		return serviceFreqExp;
	}


	public void setServiceFreqExp(float[] serviceFreqExp) {
		this.serviceFreqExp = serviceFreqExp;
		isExp=true;
	}


	public int getLorryVolume() {
		return lorryVolume;
	}


	public void setLorryVolume(int lorryVolume) {
		this.lorryVolume = lorryVolume;
	}


	public int getLorryMaxLoad() {
		return lorryMaxLoad;
	}


	public void setLorryMaxLoad(int lorryMaxLoad) {
		this.lorryMaxLoad = lorryMaxLoad;
	}


	public int getBinServiceTime() {
		return binServiceTime;
	}


	public void setBinServiceTime(int binServiceTime) {
		this.binServiceTime = binServiceTime;
	}


	public float getBinVolume() {
		return binVolume;
	}


	public void setBinVolume(float binVolume) {
		this.binVolume = binVolume;
	}


	public float getDisposalDistrRate() {
		return disposalDistrRate;
	}


	public void setDisposalDistrRate(float disposalDistrRate) {
		this.disposalDistrRate = disposalDistrRate;
	}


	public int getDisposalDistrShape() {
		return disposalDistrShape;
	}


	public void setDisposalDistrShape(int disposalDistrShape) {
		this.disposalDistrShape = disposalDistrShape;
	}


	public float getBagVolume() {
		return bagVolume;
	}


	public void setBagVolume(float bagVolume) {
		this.bagVolume = bagVolume;
	}


	public float getBagWeightMin() {
		return bagWeightMin;
	}


	public void setBagWeightMin(float bagWeightMin) {
		this.bagWeightMin = bagWeightMin;
	}


	public float getBagWeightMax() {
		return bagWeightMax;
	}


	public void setBagWeightMax(float bagWeightMax) {
		this.bagWeightMax = bagWeightMax;
	}


	public long getStopTime() {
		return stopTime;
	}


	public void setStopTime(long stopTime) {
		this.stopTime = stopTime;
	}


	public long getWarmUpTime() {
		return warmUpTime;
	}


	public void setWarmUpTime(long warmUpTime) {
		this.warmUpTime = warmUpTime;
	}


	public LinkedList<Area> getAreas() {
		return areas;
	}


	public void setAreas(LinkedList<Area> areas) {
		this.areas = areas;
	}


	public int getNoAreas() {
		return noAreas;
	}

	
	
	SimEnv()
	{
		this.lorryVolume=0;
		this.lorryMaxLoad=0;
		this.binServiceTime=0;
		this.binVolume=0;
		this.disposalDistrRate=0;
		this.disposalDistrShape=0;
		this.bagVolume=0;
		this.bagWeightMin=0;
		this.bagWeightMax=0;
		this.noAreas=0;
		this.stopTime=0;
		this.warmUpTime=0;
		
		areas=new LinkedList<Area>();
	}
	
	
	SimEnv(short lorryVolume,int lorryMaxLoad,int binServiceTime,float binVolume,float disposalDistrRate,short disposalDistrShape,float bagVolume,float bagWeightMin,float bagWeightMax,short noAreas,long stopTime,long warmUpTime)
	{
		this.lorryVolume=lorryVolume;
		this.lorryMaxLoad=lorryMaxLoad;
		this.binServiceTime=binServiceTime;
		this.binVolume=binVolume;
		this.disposalDistrRate=disposalDistrRate;
		this.disposalDistrShape=disposalDistrShape;
		this.bagVolume=bagVolume;
		this.bagWeightMin=bagWeightMin;
		this.bagWeightMax=bagWeightMax;
		this.noAreas=noAreas;
		this.stopTime=stopTime;
		this.warmUpTime=warmUpTime;
		
		areas=new LinkedList<Area>();
	}

	public void addArea(Area area)
	{
		areas.add(area);
		this.noAreas++;
	}

	//A function to print attribute values to aid in debugging.
	public void printVals()
	{
		System.out.println("lorryVolume: "+lorryVolume);
		System.out.println("lorryMaxLoad: "+lorryMaxLoad);
		System.out.println("binServiceTime: "+binServiceTime);
		System.out.println("binVolume: "+binVolume);
		System.out.println("disposalDistrRate: "+disposalDistrRate);
		System.out.println("disposalDistrShape: "+disposalDistrShape);
		System.out.println("bagVolume: "+bagVolume);
		System.out.println("bagWeightMin: "+bagWeightMin);
		System.out.println("bagWeightMax: "+bagWeightMax);
		System.out.println("noAreas: "+noAreas);
		System.out.println("stopTime: "+stopTime);
		System.out.println("warmUpTime: "+warmUpTime);
		
		System.out.println();
		
		for(int i=0;i<noAreas;i++)
		{
			System.out.println("Area "+areas.get(i).getAreaIdx());
			for(int j=0;j<areas.get(i).adjList.length;j++)
			{
				System.out.print(j);
				Set<Integer> keys=areas.get(i).adjList[j].keySet();
				
				for(Integer k :keys)
				{
					System.out.print("->"+k+"("+areas.get(i).adjList[j].get(k)+")");
				}
				System.out.println();
			}
			 System.out.println();
		}


		System.out.println("Bins: ");
		for(int i=0;i<noAreas;i++)
		{

			System.out.println("Area: "+areas.get(i).getAreaIdx());
			
			System.out.println(areas.get(i).getBins().size());
			


			System.out.println();
		}

	}
	
	
	
	
	
	
}

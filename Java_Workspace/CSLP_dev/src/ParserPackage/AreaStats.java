package ParserPackage;

public class AreaStats
{
	
	long travelTime;	//Time the lorry is working during the simulation.
	long warmUpTime;	//Time after which to collect statistics.
	int noOfTrips;		//Number of trips the lorry makes during the simulation.
	
	
	
	int noOfShedules;	//Number of schedules completed during the simulation.
	
	double volumeCollected;
	double weightCollected;
	
	int noBins;
	int totNoOverflow;	//Total number of overflowed bins.

	
	int areaId;
	

	public AreaStats(int areaId,int noBins,long warmUpTime)
	{
		this.travelTime=0;
		this.noOfTrips=0;
		this.noOfShedules=0;
		this.volumeCollected=0;
		
		this.totNoOverflow=0;
		
		this.warmUpTime=warmUpTime;
		this.noBins=noBins;
		this.areaId=areaId;
	}
	
	public void addToTravelTime(long tripStartTime,long tripEndTime)
	{
		if(tripStartTime>warmUpTime)	//Do nothing if data collected is before the warmUp time has elapsed.
		{
			long tripTravelTime=tripEndTime-tripStartTime;
			this.travelTime+=tripTravelTime;
			this.noOfTrips++;
		}
	}
	
	public void addShedule(long tripStartTime)
	{
		if(tripStartTime>warmUpTime)	//Collect statistics only in the steady state.
			this.noOfShedules++;
	}
	public void addVolumeCollected(double volume,long startTripTime)
	{
		if(startTripTime>warmUpTime)	//Collect statistics only in the steady state.
			this.volumeCollected+=volume;
	}
	
	public void addWeightCollected(double weight,long startTripTime)
	{
		if(startTripTime>warmUpTime)	//Collect statistics only in the steady state.
			this.weightCollected+=weight;
	}
	
	public void addOverflowedBin(long overFlowTime)
	{
		if(overFlowTime>warmUpTime)		//Collect statistics only in the steady state.
			this.totNoOverflow++;
	}

	public long getTravelTime() {
		return travelTime;
	}

	public int getNoOfTrips() {
		return noOfTrips;
	}

	public int getNoOfShedules() {
		return noOfShedules;
	}

	public double getVolumeCollected() {
		return volumeCollected;
	}

	public int getNoBins() {
		return noBins;
	}

	public int getTotNoOverflow() {
		return totNoOverflow;
	}

	public int getAreaId() {
		return areaId;
	}
	
	public double getWeightCollected()
	{
		return weightCollected;
	}
	
}

package ParserPackage;

public class Vertex 
{
	public int id;
	public int weight;
	
	public Vertex(int id,int weight)
	{
		this.id=id;
		this.weight=weight;
	}
	
}

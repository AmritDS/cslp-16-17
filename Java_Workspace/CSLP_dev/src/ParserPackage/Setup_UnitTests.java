package ParserPackage;
import java.util.*;
import java.io.*;

public class Setup_UnitTests 
{
	static SimEnv currentEnv;
	
	public static void main(String[] args)throws IOException
	{
		File UnitTestFileRaw =new File("UnitTestInputRaw.txt");
		File UnitTestFile=new File("UnitTestInput.txt");
		Parser.removeMetaData(UnitTestFileRaw,UnitTestFile);
		
		currentEnv=Parser.getInfo(UnitTestFile);
		//Create travel sheets for every location in every area.
		Setup.createTravelSheets(currentEnv);
	}

	//Check that LorryScheduleEvents are added correctly.
	public void setLorryShedules_Test()
	{
		LinkedList<Event> globalEvents=new LinkedList<Event>();

		
		//generate the lorries of the simulation.
		LinkedList<Lorry> lorries = Setup.generateLorries(currentEnv);
				
		//Add the lorry schedules to globalEvents.
		Setup.setLorryShedules(lorries,globalEvents,currentEnv);
		
		int noOfTrips=(int)(currentEnv.getStopTime()/(3600/lorries.get(0).getServiceFreq()));
		
		//Check that the correct number of lorry schedules has been added.
		if(!(noOfTrips==globalEvents.size()))
		{
			System.out.println("Error! setting lorry schedules");
		}
		
		for(int i=0;i<globalEvents.size();i++)
		{
			//Check that the event is of expected type.
			if(!(globalEvents.get(i).getEventType().equals("LorrySheduleEvent")))
			{
				System.out.println("Error! setting lorry schedules");

			}
		}
		
		
	}

	//Check that mergeLists merges lists in the correct order.
	public void mergeLists_Test()
	{
		LinkedList<Event> result=new LinkedList<Event>();
		LinkedList<Event> B=new LinkedList<Event>();
		
		for(int i=0;i<=10;i++)
			result.add(new GarbageBag(5,3,4,4,4,i));
		
		for(int i=-10;i>=0;i++)
			B.add(new GarbageBag(5,3,4,4,4,i));
		
		Setup.mergeLists(result,B);
		
		boolean flag=true;
		int i=1;
		long prev=result.get(0).getEventTime();
		while(i<result.size())
		{
			//check that time moves forward.
			if(prev>result.get(i).getEventTime())
			{
				flag=false;
				System.out.println(prev+" "+result.get(i).getEventTime());
				break;
			}
			
			prev=result.get(i).getEventTime();
			i++;
		}
		
		
		//check that time moves forward.
		if(!flag)
		{
			System.out.println("Error! merging lists");

		}
		
		
	}
}

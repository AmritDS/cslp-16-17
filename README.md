# README #
### Stochastic simulation of garbage disposal and collection in a smart city. ###

Code in this repository performs stochastic simulation of garbage bag disposal and collection in a city according to input parameters.  

This repository is an implementation to the specification provided at: http://www.inf.ed.ac.uk/teaching/courses/cslp/handout/cslp-2016-17.pdf  
And was completed as part of the Computer Science Large Practical (2016-2017) course, at The University of Edinburgh.  
### Getting started ###  
To run:  
1.Clone the repository.  
2.Run the compile.sh script  
3.Run the simulate.sh script with an input file as detailed here -http://www.inf.ed.ac.uk/teaching/courses/cslp/handout/cslp-2016-17.pdf  
 (Note testInput.txt is a valid file to test the simulation on.)  

Further information can be found in Readme.pdf  
and Report.pdf which documents the code structure and design.  
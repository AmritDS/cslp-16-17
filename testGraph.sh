#!/bin/bash
# This script will be used to test the simulator application

#Define colors used
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

#Compile java programs
source compile.sh

#Test for graph connectedness

#Change directory to the folder containing input files.
cd tests/disconnectedGraphs
#Store all input filenames in a file
find -name "*.txt" > ../../testFiles.txt

cd ../..

#Run simulate.sh for each file and report results.
while read line
do
     source simulate.sh tests/disconnectedGraphs/"${line:2}" >results.txt
     
     if grep -Fq "Error" results.txt
     then
            echo "${line:2} disconnected graph detected - ${green}Pass${reset}"     
     else
            echo "${line:2} disconnected graph not detected -${red}Fail${reset}"
fi
done < testFiles.txt

printf '\n'
